package main

import (
    "fmt"
    "net/http"
    "io/ioutil"
)

func handler(w http.ResponseWriter, r *http.Request) {
    file := r.URL.Path
    fmt.Printf("%s\n", file)
    if file[len(file) - 1] == '/' {
        file = file+"index.html"
    }
    file = "."+file
    contents, err := ioutil.ReadFile( file )
    if err != nil {
        fmt.Fprintf(w, "404")
        return
    }
    if file[len(file)-5:len(file)-0] == ".html" {
        w.Header().Set("Content-Type", "text/html")
    } else if file[len(file)-3:len(file)-0] == ".js" {
        w.Header().Set("Content-Type", "text/javascript")
    } else if file[len(file)-4:len(file)-0] == ".css" {
        w.Header().Set("Content-Type", "text/css")
    }
    fmt.Fprintf(w, "%s\n", contents)
}

func main() {
    http.HandleFunc("/", handler)
    http.ListenAndServe(":8080", nil)
}
